from app.lib import getUsers, addUser, deleteUser
import argparse
import re
import sys
import getpass

parser = argparse.ArgumentParser()
parser.add_argument('option', help='createuser, deleteuser, listusers')
parser.add_argument('--username', help='Username of user to create')

args = parser.parse_args()

option = args.option
if option == 'createuser':
	username = args.username
	while not username:
		try:
			user_input = input('Enter a username (\'q\' to quit): ')
		except KeyboardInterrupt:
			print()
			sys.exit(0)

		if user_input:
			if user_input == 'q':
				sys.exit(0)

			username = user_input

	users = getUsers()
	if username in users:
		print('Username already exists')
	else:
		if re.search(r'^[0-9a-z]+$',username):
			username = username
		else:
			print('Username can only contain alphanumeric characters')

	try:
		password = getpass.getpass('Password: ')
		confirmpassword = getpass.getpass('Confirm Password: ')
	except KeyboardInterrupt:
		print()
		sys.exit(0)

	if password == confirmpassword:
		addUser(username,password)
		print('User "{}" created'.format(username))
	else:
		print('Error: Passwords do not match')
elif option == 'deleteuser':
	username = args.username
	while not username:
		try:
			user_input = input('Enter a username (\'q\' to quit): ')
		except KeyboardInterrupt:
			print()
			sys.exit(0)

		if user_input:
			if user_input == 'q':
				sys.exit(0)

			username = user_input

	users = getUsers()
	if username in users:
		deleteUser(username)
		print('User "{}" deleted'.format(username))
	else:
		print('Error: User "{}" not found'.format(username))
elif option == 'listusers':
	users = getUsers()
	for username in users:
		print(username)