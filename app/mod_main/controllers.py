from flask import Blueprint, render_template

mod_main = Blueprint('main', __name__, url_prefix='/main')

@mod_main.route('/')
def index():
	return render_template('main/index.html')