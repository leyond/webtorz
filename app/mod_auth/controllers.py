from flask import Blueprint, request, render_template, redirect, url_for
from flask_login import login_user, logout_user
from werkzeug.security import check_password_hash
from app.mod_auth.forms import LoginForm
from app.mod_auth.models import User
from app.lib import getUsers, login_exempt
import app

mod_auth = Blueprint('auth', __name__, url_prefix='/auth')

@mod_auth.route('/login', methods=['GET','POST'])
@login_exempt
def login():
	form = LoginForm(request.form)
	error = None

	if form.validate_on_submit():
		username = request.form.get('username')
		password = request.form.get('password')
		rememberme = request.form.get('rememberme',False)

		users = getUsers()

		password_hash = users.get(username,None)
		if password_hash and check_password_hash(password_hash,password):
			user = User(username)
			login_user(user,remember=rememberme)
			return redirect(url_for('main.index'))
		else:
			error = 'Incorrect username or password'

	return render_template('auth/login.html', form=form, error=error)

@mod_auth.route('/logout')
def logout():
	logout_user()
	return redirect(url_for('auth.login'))