from flask import Blueprint, render_template, jsonify, request, Response
from app.lib import isFolder, isFile, deleteFolder
import os
import json
import gevent
import config

mod_file = Blueprint('file', __name__, url_prefix='/file')

@mod_file.route('/')
def index():
	return render_template('file/file.html')

@mod_file.route('/all')
def all():
	results = []

	with os.scandir(config.DOWNLOAD_FOLDER) as it:
		for entry in it:
			if entry.is_dir():
				name = os.path.join(config.DOWNLOAD_FOLDER,entry.name)
				info_filename = os.path.join(name,'torrent_info.json')
				if isFile(info_filename):
					with open(info_filename,'r') as file:
						info = json.load(file)
						info_hash = info.get('info_hash',None)
						name = info.get('name',None)

						if info_hash and name and 'files' in info:
							results.append({
								'info_hash': info_hash,
								'name': name
							})

	return jsonify(results)

@mod_file.route('/delete', methods=['POST'])
def delete():
	data = request.get_json()
	info_hash = data.get('info_hash',None)

	success = False

	if info_hash:
		folder = os.path.join(config.DOWNLOAD_FOLDER,info_hash)
		info_filename = os.path.join(folder,'torrent_info.json')
		if isFile(info_filename):
			with open(info_filename,'r') as file:
				info = json.load(file)

				if info.get('info_hash',None) == info_hash:
					success = True
					deleteFolder(folder)

	results = {
		'info_hash': info_hash,
		'success': success
	}

	return jsonify(results)

@mod_file.route('/view', defaults={'info_hash': None})
@mod_file.route('/view/<info_hash>')
def view(info_hash):
	files = []

	if info_hash:
		folder = os.path.join(config.DOWNLOAD_FOLDER,info_hash)
		info_filename = os.path.join(folder,'torrent_info.json')
		if isFile(info_filename):
			with open(info_filename,'r') as file:
				info = json.load(file)

				if info.get('info_hash',None) == info_hash:
					files = info.get('files',{})

	results = {
		'info_hash': info_hash,
		'files': files
	}

	return jsonify(results)

@mod_file.route('/download')
def download():
	data = request.args
	info_hash = data.get('info_hash',None)
	path = data.get('path',None)

	response = ('',404)

	if info_hash and path:
		folder = os.path.join(config.DOWNLOAD_FOLDER,info_hash)
		info_filename = os.path.join(folder,'torrent_info.json')
		if isFile(info_filename):
			with open(info_filename,'r') as file:
				info = json.load(file)
				files = info.get('files',{})

				file = files.get(path,None)
				size = file['size']

				if file:
					filename = os.path.join(folder,path)
					if isFile(filename):
						def generate(filename):
							with open(filename,'rb') as file:
								while True:
									data = file.read(1024)
									if not data:
										break

									yield data
									gevent.sleep(0)

						response = Response(generate(filename), mimetype='application/octet-stream')
						response.headers['Content-Disposition'] = 'attachment; filename={}'.format(os.path.basename(filename))
						response.headers['Content-Length'] = size

	return response