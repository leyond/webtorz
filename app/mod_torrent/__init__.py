from concurrent.futures import ThreadPoolExecutor
from app.lib import deleteFolder, md5File, getFileSize
import libtorrent
import uuid
import os
import re
import json
import app
import config
import time
import config

session = libtorrent.session()
session.listen_on(6881,6891)

torrents = {}

executor = ThreadPoolExecutor(config.WORKERS)

def create_torrent(url):
	global torrents

	uid = uuid.uuid4().hex
	folder = os.path.join(config.TEMP_FOLDER,'webtorz_download_{}'.format(uid))

	handler = session.add_torrent({
		'url': url,
		'save_path': folder,
		'flags': libtorrent.add_torrent_params_flags_t.flag_paused
	})

	torrents[uid] = {
		'handler': handler,
		'future': None
	}

	return uid

def download_torrent(uid):
	global torrents

	handler = torrents[uid]['handler']
	folder = handler.save_path()

	handler.resume()

	while not handler.is_seed() and uid in torrents:
		time.sleep(1)

	if uid in torrents:
		name = handler.name()

		torrents.pop(uid,None)
		session.remove_torrent(handler)

		info_hash = str(handler.info_hash())

		savefolder = os.path.join(config.DOWNLOAD_FOLDER,info_hash)
		deleteFolder(savefolder)

		files_info = {}
		for root, dirs, files in os.walk(folder):
			for f in files:
				filename = os.path.join(root,f)
				md5 = md5File(filename)
				if md5:
					size = getFileSize(filename)

					pattern = os.path.join(re.escape(folder),'(.*)')
					search = re.search(re.compile(pattern),filename)
					if search:
						filename = search.group(1)

						files_info[filename] = {
							'md5': md5,
							'size': size
						}

		info_filename = os.path.join(folder,'torrent_info.json')
		with open(info_filename,'w') as file:
			json.dump({
				'info_hash': info_hash,
				'name': name,
				'files': files_info
			},file,indent=4)

		os.rename(folder,savefolder)
	else:
		deleteFolder(folder)

def add_torrent(url):
	global torrents, executor

	uid = create_torrent(url)

	future = executor.submit(download_torrent,uid)
	torrents[uid]['future'] = future

def delete_torrent(uid):
	global session, torrents, pool

	if uid in torrents and torrents[uid]['future']:
		torrent = torrents.pop(uid)

		handler = torrent['handler']
		session.remove_torrent(handler)

		future = torrent['future']
		future.cancel()
	else:
		torrent = None

	return torrent