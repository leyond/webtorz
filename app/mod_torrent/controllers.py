from flask import Blueprint, render_template, jsonify, request
import re
import app

mod_torrent = Blueprint('torrent', __name__, url_prefix='/torrent')

@mod_torrent.route('/all')
def all():
	results = []

	torrents = app.mod_torrent.torrents

	for key in torrents:
		torrent = torrents[key]
		handler = torrent.get('handler',None)
		future = torrent.get('future',None)

		if handler and future:
			name = handler.name()
			status = handler.status()

			if handler.is_paused():
				state = 0
			else:
				state = status.state

			results.append({
				'uid': key,
				'info_hash': str(handler.info_hash()),
				'name': name,
				'download_rate': status.download_rate,
				'upload_rate': status.upload_rate,
				'peers': status.num_peers,
				'state': state,
				'total_wanted_done': status.total_wanted_done,
				'total_wanted': status.total_wanted
			})

	return jsonify(results)

@mod_torrent.route('/add', methods=['POST'])
def add():
	data = request.get_json()
	url = data.get('url',None)

	uid = None
	success = False

	torrents = app.mod_torrent.torrents

	if url:
		if url.startswith('magnet:?'):
			hashes = []

			pattern = re.compile(r'xt(?:\\.\d+)?=urn:(?:[a-z0-9]+)?:([A-Z0-9]+)',re.IGNORECASE)
			for match in pattern.finditer(url):
				hashes.append(match.group(1))

			if hashes:
				found = False

				for key in torrents:
					handler = torrents[key]['handler']
					info_hash = str(handler.info_hash())

					for h in hashes:
						if h == info_hash:
							found = True
							break

					if found:
						break

				if not found:
					success = True

					app.mod_torrent.add_torrent(url)

	results = {
		'uid': uid,
		'success': success
	}

	return jsonify(results)

@mod_torrent.route('/delete', methods=['POST'])
def delete():
	data = request.get_json()
	uid = data.get('uid',None)

	if uid:
		success = app.mod_torrent.delete_torrent(uid) != None
	else:
		success = False

	results = {
		'uid': uid,
		'success': success
	}

	return jsonify(results)