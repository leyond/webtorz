from werkzeug.security import generate_password_hash
import os
import shutil
import hashlib
import config

def login_exempt(f):
	f.login_exempt = True
	return f

def deleteFolder(folder):
	try:
		shutil.rmtree(folder)
	except OSError:
		pass

def createFolder(folder,deleteExisting=False):
	if deleteExisting:
		deleteFolder(folder)

	try:
		os.makedirs(folder)
	except OSError:
		pass

def md5File(filename):
	md5 = None

	with open(filename,'rb') as file:
		m = hashlib.md5()

		for chunk in iter(lambda: file.read(4096), b''):
			m.update(chunk)

		md5 = m.hexdigest()

	return md5

def getFileSize(filename):
	try:
		size = os.path.getsize(filename)
	except OSError:
		size = None

	return size

def isFile(filename):
	return os.path.isfile(filename)

def isFolder(folder):
	return os.path.isdir(folder)

def getUsers():
	users = {}

	filename = config.USERS_FILE
	if isFile(filename):
		with open(filename,'r') as file:
			for line in file:
				line = line.rstrip('\n')
				linesplit = line.split(':',1)
				if len(linesplit) == 2:
					username = linesplit[0]
					password = linesplit[1]
					users[username] = password

	return users

def addUser(username,password):
	filename = config.USERS_FILE
	password_hash = generate_password_hash(password,method='pbkdf2:sha256')
	with open(filename,'a') as file:
		file.write('{}:{}\n'.format(username,password_hash))

def deleteUser(username):
	users = getUsers()
	users.pop(username,None)

	filename = config.USERS_FILE
	if isFile(filename):
		with open(filename,'w') as file:
			for username in users:
				file.write('{}:{}\n'.format(username,users[username]))