from flask import Flask, redirect, url_for, request, current_app
from flask_login import LoginManager, current_user
from flask_wtf.csrf import CSRFProtect

flaskapp = Flask(__name__)
flaskapp.config.from_object('config')

login_manager = LoginManager()
login_manager.init_app(flaskapp)
login_manager.login_view = 'auth.login'

csrf = CSRFProtect(flaskapp)

from app.mod_main.controllers import mod_main as main_module
flaskapp.register_blueprint(main_module)

from app.mod_torrent.controllers import mod_torrent as torrent_module
flaskapp.register_blueprint(torrent_module)

from app.mod_file.controllers import mod_file as file_module
flaskapp.register_blueprint(file_module)

from app.mod_auth.controllers import mod_auth as auth_module
flaskapp.register_blueprint(auth_module)

from app.mod_auth.models import User
from app.lib import getUsers

@login_manager.user_loader
def load_user(user_id):
	users = getUsers()

	if user_id in users:
		user = User(user_id)
	else:
		user = None

	return user

@flaskapp.before_request
def check_auth():
	if not request.endpoint or request.endpoint.rsplit('.',1)[-1] == 'static':
		return

	user = current_user

	view = current_app.view_functions[request.endpoint]
	if not getattr(view,'login_exempt',False):
		if not user.is_authenticated:
			return redirect(url_for(login_manager.login_view))

@flaskapp.route('/')
def index():
	return redirect(url_for('main.index'))