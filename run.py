from gevent import monkey
monkey.patch_all()

from gevent.pywsgi import WSGIServer
from app import flaskapp
from app.lib import createFolder
import config

if __name__ == '__main__':
	createFolder(config.DOWNLOAD_FOLDER)

	if config.SSL_CERTIFICATE_FILE and config.SSL_PRIVATEKEY_FILE:
		http_server = WSGIServer(('0.0.0.0',5000),flaskapp,keyfile=config.SSL_PRIVATEKEY_FILE,certfile=config.SSL_CERTIFICATE_FILE)
	else:
		http_server = WSGIServer(('0.0.0.0',5000),flaskapp)

	try:
		http_server.serve_forever()
	except KeyboardInterrupt:
		pass